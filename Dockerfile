FROM python:3.6

RUN mkdir /app
WORKDIR /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY . ./

ENV FLASK_APP app.py

ENV PORT 5000
EXPOSE 5000


CMD ["flask"]
