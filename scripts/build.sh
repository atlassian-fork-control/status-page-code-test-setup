#!/usr/bin/env sh

docker-compose pull postgres redis
docker-compose up -d postgres redis
docker-compose build
