from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from celery import Celery

import os

class DevelopmentConfig:
    SECRET_KEY = "thing"
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.getenv('POSTGRES_URL')
    REDIS_URL = os.getenv('REDIS_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class TestConfig:
    SECRET_KEY = "thing"
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.getenv('POSTGRES_URL', 'postgres://postgres@localhost:5432/postgres')
    REDIS_URL = os.getenv('REDIS_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

db = SQLAlchemy()

celery = Celery(__name__, broker=os.getenv('REDIS_URL'), backend=os.getenv('REDIS_URL'))

def gen_app(config=DevelopmentConfig()):
    # App configuration
    flask_app = Flask(__name__)
    flask_app.config.from_object(config)

    from test_app.main.web import web
    flask_app.register_blueprint(web)

    from test_app.models.item import Item

    db.init_app(flask_app)

    return flask_app
