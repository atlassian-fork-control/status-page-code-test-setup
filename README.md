# Statuspage Coding Test Setup

Welcome potential future Statuspager! We're thrilled to have you come onsite soon to talk to us :)

We've tried hard to make our interview process as relevant as possible to the work you'll be doing, which means that we use some software you may not have on your laptop yet; this repo will help you make sure those dependencies are set up correctly.

So, please complete the steps below on the laptop you'll be bringing in; being able to use your own laptop set up in the way that works best for you will give you the best chance of showing us your skills!

## Requirements

* [Docker](https://docs.docker.com/engine/installation/) - You'll need docker-engine and docker-compose installed (you don't need to know Docker well - we've written some scripts to abstract over it)

## Test that everything works

In terminal 1:

```
./scripts/build.sh && ./scripts/migrate.sh && ./scripts/up.sh
# you should see "Running on http://0.0.0.0:5000/"
```

In terminal 2:

```
./scripts/test.sh
# you should see 1 test pass

curl -X POST http://localhost:5000/
# you should see "Docker and docker-compose are set up correctly"

cat <<INPUT | ./scripts/console.sh
from test_app.models.item import Item; print('Console works -',Item.query.count(), 'items in db'); quit()
INPUT
# you should see "Console works"
```

If you really get stuck, don't worry, you can borrow a Mac laptop from us.

# More info on the commands

In the real code test you'll be using a very similar setup (even if you're not using Python), so here's some more info on the commands you'll have available.

## Setup the system

1. `./scripts/build.sh` - builds the image, and downloads and runs postgres and redis
2. `./scripts/migrate.sh` - setups the initial database schema on postgres

## Run the system

`./scripts/up.sh` - runs the web and background processes in the foreground

## The console

`./script/console.sh` - loads a python/ruby shell, with the app and models loaded into memory

## Run the test suite

`./scripts/test.sh`  - runs the entire test suite.

## Reset your database

`./scripts/reset.sh` - reset the database to migration 0
